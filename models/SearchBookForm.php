<?php

namespace app\models;

use yii\base\Model;

class SearchBookForm extends Model
{
    public $authorId;
    public $name;
    public $dateFrom;
    public $dateTo;

    public function rules()
    {
        return [
            [['dateFrom', 'dateTo'], 'safe'],
            [['authorId'], 'integer'],
            [['name'], 'string', 'max' => 250],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название книги',
            'dateFrom' => 'Дата выхода с',
            'dateTo' => 'Дата выхода по',
            'authorId' => 'Автор',
        ];
    }

    public function generateQuery() {
        $query = Book::find();

        if (!empty($this->authorId)) {
            $query->andWhere(['author_id' => $this->authorId]);
        }

        if (!empty($this->name)) {
            $query->andWhere(['LIKE', 'name', $this->name]);
        }

        if (!empty($this->dateFrom) && !empty($this->dateTo)) {
            $query->andWhere(['BETWEEN', 'date', $this->dateFrom, $this->dateTo]);
        }

        if (!empty($this->dateFrom) && empty($this->dateTo)) {
            $query->andWhere(['BETWEEN', 'date', $this->dateFrom, date('Y-m-d')]);
        }

        if (empty($this->dateFrom) && !empty($this->dateTo)) {
            $query->andWhere(['BETWEEN', 'date', '1900-01-01', $this->dateTo]);
        }

        $query = Book::find()->joinWith('author');

        return $query;
    }
}