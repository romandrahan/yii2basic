<?php

use yii\widgets\DetailView;

?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'format' => 'html',
                'label' => 'Превью',
                'value' => '<img class="book-image" src="images/' . $model->preview . '">',
            ],
            [
                'format' => 'text',
                'label' => 'Автор',
                'value' => $model->author->firstname  . " " . $model->author->lastname,
            ],
            [
                'attribute' => 'date',
                'format' => ['Date', 'long'],
                'label' => 'Дата выхода книги',
            ],
            [
                'attribute' => 'date_create',
                'format' => 'RelativeTime',
                'label' => 'Дата добавления',
            ],
        ],
    ]) ?>

</div>
