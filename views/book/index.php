<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Author;

$this->title = 'Книги';
$this->params['breadcrumbs'][] = $this->title;
?>


<?php $form = ActiveForm::begin(['method' => 'GET']); ?>

<?= $form->field($searchForm, 'authorId')->dropDownList(
    ArrayHelper::map(Author::find()->all(), 'id', function($model) {
        return $model->firstname . ' ' . $model->lastname;
    }), array('prompt'=>'Выберите автора')
)
?>

<?= $form->field($searchForm, 'name')->textInput(['maxlength' => true]) ?>

<?= $form->field($searchForm, 'dateFrom')->textInput(['placeholder' => 'ГГГГ-ММ-ДД']) ?>

<?= $form->field($searchForm, 'dateTo')->textInput(['placeholder' => 'ГГГГ-ММ-ДД']) ?>

<div class="form-group">
    <?= Html::submitButton('Искать', ['class' => 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'emptyText' => 'Нет книжек',
    'summary' => '',
    'columns' => [
        'id',
        'name',
        [
            'format' => 'html',
            'label' => 'Превью',
            'value' => function($model) { return '<img class="book-image" src="images/' . $model->preview . '">'; },
        ],
        [
            'attribute' => 'author',
            'format' => 'text',
            'label' => 'Автор',
            'value' => function($model) { return $model->author->firstname  . " " . $model->author->lastname; },
        ],
        [
            'attribute' => 'date',
            'format' => ['Date', 'long'],
            'label' => 'Дата выхода книги',
        ],
        [
            'attribute' => 'date_create',
            'format' => 'RelativeTime',
            'label' => 'Дата добавления',
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'buttons' => [
                'view' => function ($url, $model, $key) {
                    return Html::a(
                        '<span class="glyphicon glyphicon-search" aria-hidden="true"></span>',
                        ['book/view', 'id' => $key],
                        [
                            'class'=>'showModalLink',
//                                'value' => Url::to(['book/view', 'id' => $key]),
                            'data-toggle' => 'modal',
                            'data-target' => '#viewBookModal',
                        ]);
                },
            ]
        ],
    ],
]); ?>

<?php
Modal::begin([
    'id' => 'viewBookModal',
    'header' => '<h4 class="modal-title">Информация о книге</h4>',
    'footer' => '<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>',
]);
Modal::end();
?>