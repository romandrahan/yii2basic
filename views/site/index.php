<?php

use yii\helpers\Html;
use yii\bootstrap\Button;

$this->title = 'Книжки';
?>
<table class="table table-bordered">
    <thead>
    <tr>
        <th>ID</th>
        <th>Название</th>
        <th>Превью</th>
        <th>Автор</th>
        <th>Дата выхода книги</th>
        <th>Дата добавления</th>
        <th>Кнопки действий</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($books as $book): ?>
        <tr>
            <td><?= $book->id ?></td>
            <td><?= Html::encode("{$book->name}") ?></td>
            <td><img class="book-image" src="images/<?= Html::encode("{$book->preview}") ?>"></td>
            <td>
                <?= Html::encode("{$book->author->firstname}") ?>
                <?= Html::encode("{$book->author->lastname}") ?>
            </td>
            <td><?= Yii::$app->formatter->asDate($book->date, 'long') ?>
            </td>
            <td><?= Yii::$app->formatter->asRelativeTime($book->date_create) ?></td>
            <td>
                <?= Html::a(
                    '<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>',
                    ['book/update', 'id' => $book->id],
                    ['class'=>'btn btn-default btn-sm'])
                ?>
                <?= Button::widget([
                    'label' => '<span class="glyphicon glyphicon-search" aria-hidden="true">',
                    'options' => ['class' => 'btn-sm btn-default'],
                    'encodeLabel' => false,
                ])
                ?>
                <?= Button::widget([
                    'label' => '<span class="glyphicon glyphicon-remove" aria-hidden="true">',
                    'options' => ['class' => 'btn-sm btn-default'],
                    'encodeLabel' => false,
                ])
                ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>