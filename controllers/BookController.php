<?php

namespace app\controllers;

use app\models\SearchBookForm;
use yii\filters\AccessControl;
use Yii;
use app\models\Book;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

class BookController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'view', 'update', 'delete'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchForm = new SearchBookForm();
        $searchForm->load(Yii::$app->request->get());

        $dataProvider = new ActiveDataProvider([
            'query' => $searchForm->generateQuery(),
        ]);

        $dataProvider->sort->attributes['author'] = [
            'asc' => ['authors.firstname' => SORT_ASC, 'authors.lastname' => SORT_ASC],
            'desc' => ['authors.firstname' => SORT_DESC, 'authors.lastname' => SORT_DESC],
        ];

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchForm' => $searchForm
        ]);
    }

    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->previewFile = UploadedFile::getInstance($model, 'previewFile');

            if ($model->previewFile != null) {
                $model->preview = Yii::$app->security->generateRandomString() . '.' . $model->previewFile->extension;
            }

            if ($model->save()) {
                if ($model->previewFile != null) {
                    $model->previewFile->saveAs('images/' . $model->preview);
                }

                return $this->redirect(Yii::$app->getUser()->getReturnUrl());
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        } else {
            Yii::$app->getUser()->setReturnUrl(Yii::$app->request->referrer);

            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->goBack();
    }

    protected function findModel($id)
    {
        if (($model = Book::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
